@extends('layouts.guest')

@section('content')
<!-- Loader -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<div class="tm-main-container">
    <div class="tm-top-container">
        <!-- Menu -->
        @include('contents.guests.menu')

        <!-- Site header -->
        <header class="tm-site-header-box tm-bg-dark">
            <h1 class="tm-site-title">The Card</h1>
            <p class="mb-0 tm-site-subtitle">Free HTML Template for you</p>
        </header>
    </div>
    <!-- tm-top-container -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Site content -->
                <div class="tm-content">
                    <!-- Section 0 Introduction -->
                    <section class="tm-section tm-section-0">
                        <h2 class="tm-section-title mb-3 font-weight-bold">
                            Introduction
                        </h2>
                        <div class="tm-textbox tm-bg-dark">
                            <p>
                                The Card template is last updated on 21 May 2019. Read More button is linked to second page. You can try it now. External link button is added on main menu.
                            </p>
                            <p class="mb-0">
                                Please tell your friends about Tooplate for free templates. This is 70% alpha background.
                            </p>
                        </div>
                        <a href="#" id="tm_about_link" data-linkid="1" class="tm-link">Read More</a>
                    </section>
                    <!-- Section 1 About Me -->
                    @include('contents.guests.sections.1')
                    <!-- Section 2 Work (Gallery) -->
                    @include('contents.guests.sections.2')
                    <!-- Section 3 Contact -->
                    @include('contents.guests.sections.3')
                </div>
            </div>
        </div>
    </div>

    <div class="tm-bottom-container">
        <!-- Barcode -->
        <div class="tm-barcode-box">
            <img src="{{ asset('vendors/thecard/img/bar-code.jpg') }}" alt="Bar code" />
        </div>
        <!-- Footer -->
        <footer>
            || Copyright &copy; 2018 Company Name ||
            Design: <a rel="nofollow" href="https://fb.com/tooplate">Tooplate</a> ||
        </footer>
    </div>
</div>
@endsection
