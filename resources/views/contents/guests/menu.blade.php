<nav id="tmNav" class="tm-nav">
    <a class="tm-navbar-menu" href="#">Menu</a>
    <ul class="tm-nav-links">
        <li class="tm-nav-item active">
            <a href="#" data-linkid="0" data-align="right" class="tm-nav-link">Intro</a>
        </li>
        <li class="tm-nav-item">
            <a href="#" data-linkid="1" data-align="right" class="tm-nav-link">About</a>
        </li>
        <li class="tm-nav-item">
            <a href="#" data-linkid="2" data-align="middle" class="tm-nav-link">Work</a>
        </li>
        <li class="tm-nav-item">
            <a href="#" data-linkid="3" data-align="left" class="tm-nav-link">Contact</a>
        </li>
        <hr>
        @if (Route::has('login'))
            @auth
            <li class="tm-nav-item">
                <a href="{{ url('/home') }}" rel="nofollow" class="tm-nav-link external">Home</a>
            </li>
            @else
            <a href="{{ route('login') }}"rel="nofollow" class="tm-nav-link external">Login</a>
                @if (Route::has('register'))
                <a href="{{ route('register') }}" rel="nofollow" class="tm-nav-link external">Register</a>
                @endif
            @endauth
        @endif
        <hr>
        <li class="tm-nav-item">
            <a rel="nofollow" href="https://fb.com/tooplate" class="tm-nav-link external">External</a>
        </li>
    </ul>
</nav>
