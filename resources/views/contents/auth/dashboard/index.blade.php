@extends('layouts.app')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Dasbor</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="container-fluid">
    <h1 class="text-black-50">You are logged in!</h1>
</div>
@endsection
